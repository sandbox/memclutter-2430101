<?php
/**
 * @file
 * Drush commands.
 */

require_once 'fias.parse.inc';

/**
 * Implement hook_drush_command().
 *
 * @return array
 */
function fias_drush_command()
{
  $items['fias-parser'] = array(
    'description' => 'Run Fias parser',
    'arguments' => array(
      'source' => 'Data source: addrobj, house, socrbase',
      'type' => 'Source type: xml, dbf',
      'location' => 'Source location: full path for source file',
    ),
    'aliases' => array('fiasp'),
  );

  return $items;
}

/**
 * Implement drush_COMMANDFILE_COMMANDNAME().
 *
 * @param $source
 * @param $type
 * @param $location
 */
function drush_fias_parser($source, $type, $location)
{
  if (!in_array($source, array('socrbase', 'addrobj', 'house'))) {
    watchdog('fias', 'Incorrect source name %source%', array('%source%' => $source), WATCHDOG_ERROR);
  }

  if (!in_array($type, array('xml', 'dbf'))) {
    watchdog('fias', 'Incorrect source type %type%', array('%type%' => $type), WATCHDOG_ERROR);
  }

  $function = "fias_parse_{$source}_{$type}";
  if (!is_callable($function)) {
    watchdog('fias', 'Not found parse function %function%', array('%function%' => $function), WATCHDOG_ERROR);
  }

  if (!file_exists($location)) {
    watchdog('fias', 'Not found source file %location%', array('%location%' => $location), WATCHDOG_ERROR);
  }

  call_user_func_array($function, array($location));
}
